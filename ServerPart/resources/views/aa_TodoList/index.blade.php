@extends('aa_TodoList.Layout.base')


@section('title','welcome')

@section('body')



    @include('ab_Partials.message')
    <h1  class="text-center">
        To do List
    </h1>
    <div class="row justify-content-center">


        <div class="col-12">


            <ul class="list-group">
                @foreach($todos as $todo)
                    <li class="list-group-item">
                        {{--
                           {{ucfirst($todo->body)}}
                        --}}

                        <div class="row">

                            <div class="col-8">
                                <a href="{{'/todo/'.$todo->id}}">
                                    {{$todo->title}}
                                </a>
                            </div>

                            <div class="col-2">
                                <a href="{{'/todo/'.$todo->id.'/edit'}}" class="btn btn-info"> Edit</a>
                            </div>
                            <div class="col-2">
                                <form class="form-group pull-right" action="{{'/todo/'.$todo->id}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}


                                    <button type="submit" class="btn btn-primary">delete</button>





                                </form>

                            </div>


                        </div>



                    </li>
                @endforeach
            </ul>
        </div>


    </div>

    <a href="todo/create" class="btn btn-info text-center"> Add new</a>

@endsection

