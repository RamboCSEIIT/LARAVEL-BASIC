@extends('aa_TodoList.Layout.base')





@section('title','welcome')

@section('body')
    <br>

    <div class="col-lg-4">
        <center><h1>{{substr(Route::currentRouteName(),5) }} List</h1></center>
        <a href="/todo" class="btn btn-info"> Back</a>
        <form class="form-horizontal" action="/todo/@yield('editId')" method="post">
            {{csrf_field()}}
            @section('editMethod')

            @show

            <fieldset>
                <div class="form-group">

                    <div class="col-lg-12">
                        <input type="text" name="title" value="@yield('editTitle')" class="form-control" placeholder="This is new title">


                    </div>
                    <div class="col-lg-12">
                        <textarea class="form-control"   name="body" id="exampleTextarea" rows="5" placeholder="This is new body">
                            @yield('editBody')
                        </textarea>
<br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </div>

            </fieldset>
        </form>

@include('ab_Partials.errors')




    </div>

@endsection


