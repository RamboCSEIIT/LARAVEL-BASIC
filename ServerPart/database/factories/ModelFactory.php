<?php

$factory->define(App\poly_video::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\role::class, function (Faker\Generator $faker) {
    return [
        'role' => $faker->word,
    ];
});

$factory->define(App\video::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'url' => $faker->url,
    ];
});

$factory->define(App\Userdummy::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'country_id' => $faker->randomNumber(),
    ];
});

$factory->define(App\todo::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'body' => $faker->word,
    ];
});

$factory->define(App\ppost::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'body' => $faker->word,
    ];
});

$factory->define(App\Models\AA_SUSTEST9\Student::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\song::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'artist' => $faker->word,
    ];
});

$factory->define(App\Passport::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->randomNumber(),
        'userdummy_id' => function () {
             return factory(App\Userdummy::class)->create()->id;
        },
    ];
});

$factory->define(App\home::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\poly_post::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\poly_tag::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\poly_taggable::class, function (Faker\Generator $faker) {
    return [
        'poly_tag_id' => $faker->randomNumber(),
        'poly_taggable_id' => $faker->randomNumber(),
        'poly_taggable_type' => $faker->word,
    ];
});

$factory->define(App\country::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\comment::class, function (Faker\Generator $faker) {
    return [
        'commentable_type' => $faker->word,
        'commentable_id' => $faker->randomNumber(),
    ];
});

$factory->define(App\mobile::class, function (Faker\Generator $faker) {
    return [
        'number' => $faker->word,
        'userdummy_id' => function () {
             return factory(App\Userdummy::class)->create()->id;
        },
    ];
});

$factory->define(App\post::class, function (Faker\Generator $faker) {
    return [
        'userdummy_id' => $faker->randomNumber(),
        'title' => $faker->word,
    ];
});

$factory->define(App\Models\AA_SUSTEST0\Student::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AA_SUSTEST19\Student::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AA_INDIA_GERMAN\Job::class, function (Faker\Generator $faker) {
    return [
        'queue' => $faker->word,
        'payload' => $faker->text,
        'attempts' => $faker->boolean,
        'reserved_at' => $faker->randomNumber(),
        'available_at' => $faker->randomNumber(),
    ];
});

$factory->define(App\Models\AA_INDIA_GERMAN1\Job::class, function (Faker\Generator $faker) {
    return [
        'queue' => $faker->word,
        'payload' => $faker->text,
        'attempts' => $faker->boolean,
        'reserved_at' => $faker->randomNumber(),
        'available_at' => $faker->randomNumber(),
    ];
});

$factory->define(App\Models\AAA_FINAL\Card::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\ZZ_TEST\Card::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\ZZ_TEST\Clas::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AA_CASE\Job::class, function (Faker\Generator $faker) {
    return [
        'queue' => $faker->word,
        'payload' => $faker->text,
        'attempts' => $faker->boolean,
        'reserved_at' => $faker->randomNumber(),
        'available_at' => $faker->randomNumber(),
    ];
});

