<?php

use Illuminate\Database\Seeder;
use App\mobile;
use App\Userdummy;

class acMobile extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        for ($i = 0; $i < Userdummy::get()->count(); $i++)
        {


            $row = Userdummy::skip($i)->take(1)->get();


           // var_dump($row[0]->id);
            DB::table('mobiles')->insert([

                'number' => $faker->phoneNumber,
                'userdummy_id' => $row[0]->id

            ]);

        }


        for ($i = 0; $i < Userdummy::get()->count(); $i++)
        {


            $row = Userdummy::skip($i)->take(1)->get();


          //  var_dump($row[0]->id);
            DB::table('mobiles')->insert([

                'number' => $faker->phoneNumber,
                'userdummy_id' => $row[0]->id,
                'created_at' => $faker->dateTime,

                'updated_at' => $faker->dateTime

            ]);

        }
    }
}
