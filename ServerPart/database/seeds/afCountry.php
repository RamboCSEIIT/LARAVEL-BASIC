<?php

use Illuminate\Database\Seeder;
use App\Userdummy;
use App\country;
class afCountry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        for( $i = 0; $i<DatabaseSeeder::$country_length; $i++ ) {


            DB::table('countries')->insert([

                'name' => $faker->country

            ]);


        }

        for($i = 0; $i<DatabaseSeeder::$user_table_length; $i++ ) {

            $country = country::skip(rand(0,country::get()->count()-1))->take(1)->get();
            $user_dummy = Userdummy::skip($i)->take(1)->get();
            Userdummy::where('id', $user_dummy[0]->id)
                ->update(['country_id' => $country[0]->id]);



        }

    }
}
