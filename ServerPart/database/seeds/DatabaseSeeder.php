<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static $user_table_length = 10;
    public static $country_length = 5;
    public static $post_length = 15;
    public static $video_length = 5;
    public static $comments_length = 10;
    public function run()
    {



        $this->call(aaFeedUser::class);
        $this->call(abFeedPassport::class);
        $this->call(acMobile::class);
        $this->call(adRoleSeeder::class);
        $this->call(aeRoleUserSeeder::class);
        $this->call(afCountry::class);
        $this->call(agPosts::class);
        $this->call(aivideo::class);
        $this->call(akppost::class);
        $this->call(ahcomments::class);
        $this->call(al_poly_post::class);
        $this->call(al_poly_video::class);
        $this->call(al_poly_tag::class);
       $this->call(al_poly_taggables::class);
        $this->call(am_todolist::class);
         $this->call(ag_user_chat::class);
    }
}
