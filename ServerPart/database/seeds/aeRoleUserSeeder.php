<?php

use Illuminate\Database\Seeder;
use App\Userdummy;
use App\role;
class aeRoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        //
        for ($i = 0; $i < Userdummy::get()->count(); $i++)
        {


            $row_user = Userdummy::skip($i)->take(1)->get();
            $row_role = role::skip(rand(0,role::get()->count()-1))->take(1)->get();

            //  var_dump($row[0]->id);
            DB::table('role_userdummy')->insert([



                'role_id' => $row_role[0]->id,
                'userdummy_id' => $row_user[0]->id

            ]);

        }

        for ($i = 0; $i < role::get()->count(); $i++)
        {


            $row_role = role::skip($i)->take(1)->get();
            $row_user = Userdummy::skip(rand(0,Userdummy::get()->count()-1))->take(1)->get();

            //  var_dump($row[0]->id);
            DB::table('role_userdummy')->insert([



                'role_id' => $row_role[0]->id,
                'userdummy_id' => $row_user[0]->id
            ]);

        }
    }
}
