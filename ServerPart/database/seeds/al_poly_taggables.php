<?php

use Illuminate\Database\Seeder;
use App\poly_taggable;
use App\poly_tag;
use App\poly_post;
use App\poly_video;

class al_poly_taggables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < DatabaseSeeder::$post_length; $i++) {

            if($i%2==0)
            {
                $vid_post = poly_post::skip(rand(0,poly_post::get()->count()-1))->take(1)->get();
                $type= "App\\poly_post";

            }
            else
            {
                $vid_post= poly_video::skip(rand(0,poly_video::get()->count()-1))->take(1)->get();
                $type= "App\\poly_video";


            }

            $tag = poly_tag::skip(rand(0,poly_tag::get()->count()-1))->take(1)->get();

            DB::table('poly_taggables')->insert([
                'poly_tag_id' => $tag[0]->id,
                'poly_taggable_id' => $vid_post[0]->id,
                'poly_taggable_type' =>$type
            ]);


        }
    }
}
