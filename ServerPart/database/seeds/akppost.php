<?php

use Illuminate\Database\Seeder;

class akppost extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        //
        for ($i = 0; $i < DatabaseSeeder::$post_length; $i++) {

            DB::table('pposts')->insert([
                'title' =>"posts__".$faker->address,
                'body' => $faker->paragraph
            ]);


        }
    }

}
