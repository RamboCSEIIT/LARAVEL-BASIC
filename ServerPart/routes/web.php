<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\mobile;
use Illuminate\Http\Request;

use App\Userdummy;
use App\Http\Middleware\aa_Sample\test;
use App\Models\aa_search\search;

//Route::get('/','homeController@index');
Route::get('about','homeController@about');
Route::resource('songs','pages\SongsController');
Route::resource('todo','aa_TodoList\todoController');

Route::get('middleware1','ab_Middleware\test@middleware1')->middleware('test:rejoy');
Route::get('middleware2','ab_Middleware\test@middleware2');
Route::get('sendmail','ac_MailController\mailController@sendmailM');
Route::get('sendmailT','ac_MailController\mailController@sendmail');
Route::get('testemail','ac_MailController\mailController@testmail');
Route::post('testemailpost','ac_MailController\mailController@testmailpost');

Route::get("/form",'ae_Recapitcha\recaptichaController@index');
Route::post("/form",'ae_Recapitcha\recaptichaController@store');
//Route::get("/",'af_Algolia\algolia@algolia');

Route::get("/local1/{lang?}",function ($lang=null){
    App::setlocale($lang);
    return view("ae_Recapitcha.index");
});

Route::get("/event",function (){

    event(new \App\Events\TaskEvent("Hey How are you"));
});

Route::get("/listener",function (){

    return view('af_EventListner.index');
});

Route::get("/",function (){

    return view('home');
});
/*
Route::get('middleware',function(){

    $mobiles = Userdummy::find(5)->mobile;
    $user=mobile::find(11)->Userdummy;
    return view('aa_Sample.middleware',[
        'user'=>$user,
        'mobiles'=>$mobiles]);
})->middleware('test');

Route::get('contact',function(){

    $mobiles = Userdummy::find(5)->mobile;
    $user=mobile::find(11)->Userdummy;
    return view('aa_Sample.contact',[
        'user'=>$user,
        'mobiles'=>$mobiles]);
})->middleware('test');
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get("/aa_case","AA_CASE\index@_aa_case");