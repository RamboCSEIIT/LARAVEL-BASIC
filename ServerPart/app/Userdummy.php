<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Userdummy extends Model
{
    //
    public  function passport(){
        return $this->hasOne(Passport::class);
    }

    public  function mobile(){
        return $this->hasMany(mobile::class);
    }

    public  function role(){
        return $this->belongsToMany(role::class)->withPivot('created_at','updated_at');
    }
}
