<?php

namespace App\Mail\testmail;

use http\Env\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use function var_dump;

class mail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private    $data;
    public function __construct(  $data )
    {


        $this->to($data["from"],$data["from_name"]);
       // $this->from($data["to"],$data["to_name"]);
        $this->from($data["to"],$data["to_name"]);
        $this->subject($data["title"]);

        $this->data=$data;


    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->data['message']."_______".$this->data['to']);
       // var_dump($this->data["message"]);
       // dd();
       return $this->view('ac_Mail.index',['mailmessage'=>$this->data["message"]]);
    }
}

