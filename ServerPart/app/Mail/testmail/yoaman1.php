<?php

namespace App\Mail\testmail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class yoaman1 extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private    $data;
    public function __construct(  $data )
    {


        $this->to($data["from"],$data["from_name"]);
        // $this->from($data["to"],$data["to_name"]);
        $this->from($data["to"],$data["to_name"]);
        $this->subject($data["title"]);

        $this->data=$data;


    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('yomanX');
    }
}
