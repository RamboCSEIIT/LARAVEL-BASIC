<?php

namespace App\Http\Middleware\aa_Sample;

use Closure;

class test1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $ip = $request->ip();
        if($ip=="127.09g.0.1")
        {
            //throw new \Exception("Your ip is correct");

            return redirect('/');

        }
        return $next($request);
    }
}
