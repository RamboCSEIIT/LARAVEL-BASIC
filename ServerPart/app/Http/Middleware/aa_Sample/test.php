<?php

namespace App\Http\Middleware\aa_Sample;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$name)
    {
        dd($name);
         $ip = $request->ip();
         if($ip=="127.8.0.1")
         {
             //throw new \Exception("Your ip is correct");

             return redirect('/');

         }

        return $next($request);
    }
}
