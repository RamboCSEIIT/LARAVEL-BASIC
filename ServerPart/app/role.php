<?php

namespace App;
use App\Userdummy;
use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    public  function users(){
        return $this->belongsToMany(Userdummy::class)->withTimestamps();
    }
}
