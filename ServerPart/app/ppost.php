<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\comment;

class ppost extends Model
{
    public function comments()
    {
        return $this->morphMany('App\comment','commentable');
    }
}
