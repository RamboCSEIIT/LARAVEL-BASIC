<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class poly_post extends Model
{
   public function tags()
   {
       return $this -> morphToMany('App\poly_tag','poly_taggable');
   }
}
