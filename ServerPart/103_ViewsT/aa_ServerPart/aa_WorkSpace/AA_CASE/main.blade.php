@extends('aa_ServerPart.aa_WorkSpace.AA_CASE.base')

@section('title')
    Test
@endsection

@section('cssBlock')
    @include('aa_ServerPart.aa_WorkSpace.AA_CASE.aa_include.za_css')
@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('aa_ServerPart.aa_WorkSpace.AA_CASE.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('aa_ServerPart.ab_body.AA_CASE.body')




@endsection


@section('bottomJS')

    @include('aa_ServerPart.aa_WorkSpace.AA_CASE.aa_include.zb_javascript')

@endsection


 
  