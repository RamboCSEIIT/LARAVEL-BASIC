var VALID =1;
var IGNORE =0;
var gulp = require('gulp');
//Directory Structire
var requireDir       = require('require-dir');
var common_tasks     = requireDir('./zz_tasks/zz_COMMON_TASKS');
var env_tasks        = requireDir('./zz_tasks/zz_ENV_VARIABLES');
var webpage_tasks    = requireDir('./zz_tasks/aa_WEBPAGES');
var core_tasks       = requireDir('./zz_tasks');



///// NEW PAGE ADJUST TASKS ////////////////////////////////////////////////////////////////////////
//web pages

gulp.task('aa_case_set_env' , webpage_tasks.aa_case_set_env.aa_case_mode);

process.env.TEST_PAGE  = "http://127.0.0.1:8000/aa_india_german";

global.items = [
    ['aa_case_set_env', VALID]
];
/////////////////////////////////////////////////////////////////////////////////////////





var SASS_CHECK_PATH                                          = '00_SASS/**/*.*';
var SCRIPTS_CHECK_PATH                                    = '01_SCRIPTS/**/*.*';
 
var PHP_PATH_BOOT                                       = 'BootingFiles/**/*.*';
var PHP_PATH_SRC                                    = 'ServerPart/src/**/*.php';
var VIEW_T_WORK_SPACE              = 'ViewsT/aa_ServerPart/aa_WorkSpace/**/*.*';
var VIEW_PATH_FRAGMENT                           = 'ViewsT/ab_Fragments/**/*.*';
 


 

 
global.printMsg = false;


var gulpSequence = require('gulp-sequence');
// process.env.NODE_ENV
 var util = require('gulp-util');














//global variable
global.browserSync = require('browser-sync').create();

//Common tasks
gulp.task('clear'             , common_tasks.b_miscellaneous.clear);
gulp.task('testG'             , common_tasks.c_clean.test_global);

gulp.task('browser-init'      , common_tasks.a_browser.browser_init);
gulp.task('browser-reload'    , common_tasks.a_browser.browser_reload);
gulp.task('javaScriptError'   , core_tasks.a_javascript.javaScriptError);


///Environment variables
gulp.task('set-mode-dev-env'    , env_tasks.common_env.set_mode_dev_env);
gulp.task('set-mode-deploy-env' , env_tasks.common_env.set_mode_deploy_env);

 
  
 // miscellaneous
gulp.task('clean-without-image' , common_tasks.c_clean.clean_without_image);
gulp.task('clean-with-image'    , common_tasks.c_clean.clean_with_image);

//Common specific
gulp.task('javascript-move'     , core_tasks.a_javascript.javascript_move);
gulp.task('sass-move'           , core_tasks.b_sass.sass_move);
gulp.task('html5-body-move'     , core_tasks.c_html.html5_body_move);
gulp.task('html5-minify-view'   , core_tasks.c_html.html5_minify_view);
gulp.task('image-move'          , core_tasks.d_images.image_move); 







 
 
gulp.task('pages',function (cb)
{
    initialize_page_tasks();
    console.log(global.task_pages_sass_js_body);
    gulpSequence(global.task_pages_sass_js_body,'html5-minify-view')(cb);

    
});

 

gulp.task('pages-watch',function (cb)
{
 
     
    gulpSequence('pages','browser-reload')(cb);

    
});

 



 

 


    gulp.task('watch', function ( )
    {
        console.log("Inside XXXXXXXXXXXXXXXXXXXXXXXXX");

        if(util.env.name == undefined)
        {

                gulp.watch([PHP_PATH_BOOT,PHP_PATH_SRC], function (  )
                {

                    gulp.start('browser-reload');


                });


                gulp.watch([SASS_CHECK_PATH,SCRIPTS_CHECK_PATH,VIEW_T_WORK_SPACE,VIEW_PATH_FRAGMENT], function ()
                {


                    gulp.start('pages-watch');




                });

        }







        return;
    });


    gulp.task('dev', function (cb)
    {

        gulpSequence(
            'clear',
            'clean-without-image',
            'set-mode-dev-env',
            'pages',
            'browser-init',
            'watch')(cb);


    });


    gulp.task('dep', function (cb)
    {

        gulpSequence(
            'clear',
            'clean-without-image',
            'set-mode-deploy-env',
            'pages',
            'browser-init',
            'watch')(cb);


    });





 

 


//gulp make-page  --name "az_curd_page"
gulp.task('make-page', function() 
{
    var DNAME = (util.env.name).toUpperCase();






    var options  = {

                    SASS_IN:"000_TEMPLATE/00_SASS" ,
                    SASS_OUT:"100_SASS/"+DNAME ,
                    SCRIPTS_IN:"000_TEMPLATE/01_SCRIPTS" ,
                    SCRIPTS_OUT:"101_SCRIPTS/"+DNAME ,
                    IMAGES_IN:"000_TEMPLATE/02_IMAGES" ,
                    IMAGES_OUT:"102_IMAGES/"+DNAME,
                    WORKSPACE_IN:"000_TEMPLATE/03_WORK_SPACE" ,
                    WORKSPACE_OUT:"103_ViewsT/aa_ServerPart/aa_WorkSpace/"+DNAME,
                    FRAGMENTS_IN:"000_TEMPLATE/04_FRAGMENTS" ,
                    FRAGMENTS_OUT:"103_ViewsT/ab_Fragments/"+DNAME,

                    GULP_IN:"000_TEMPLATE/05_GULP_PAGE" ,
                    GULP_OUT:"zz_tasks/aa_WEBPAGES",

                    PHP_IN:"000_TEMPLATE/06_PHP_PAGE_CLASS" ,
                    PHP_OUT:"ServerPart/src/Controllers",


                   'NAME':util.env.name,
                   'DNAME':DNAME,
                   'DBASE':util.env.dbase,



                };

    console.log(options );


//
         return make_template(options);
            
});
//gulp del-page  --name "az_curd_page"



gulp.task('del-page', function() 
{

var DNAME = (util.env.name).toUpperCase();
var NAME =  util.env.name;





    var dir = [
        ['SASS_CLEAN','100_SASS/'+DNAME ],
        ['SCRIPTS_CLEAN','101_SCRIPTS/'+DNAME ],
        ['VIEW_CLEAN','103_ViewsT'],
        ['CONTROLLER_CLEAN','app/Http/Controllers/'+DNAME],
        ['MODEL_CLEAN','app/Models/'+DNAME]





    ];
 
var files = [





    ['SEED_CLEAN','database/seeds/'+NAME+".php"],
    ['MIGRATION_CLEAN','database/migrations/*'+NAME+"*.php"]



];
                
                
                       return remove_template(dir,files,NAME);


 
            
});

gulp.task('make-database', function(cb)
{

    var DNAME = (util.env.name).toUpperCase();

    var options_script_page = {


        'NAME':util.env.name,
        'DNAME':DNAME,
        'DIR':util.env.dir,
        'DBASE':util.env.dbase

    };


    return make_data_base(options_script_page);


});




