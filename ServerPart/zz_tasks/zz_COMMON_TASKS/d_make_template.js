
var options;
var gulpSequence = require('gulp-sequence');
var gulp                                                      = require('gulp');
const template = require('gulp-template');
var insert = require('gulp-insert');
var concat                                             = require('gulp-concat');

const fs = require('fs-extra');
const fs_s = require('fs');



global.make_template = function (options)
{
    
   
     var result_scripts=gulp.src(options.SCRIPTS_IN+"/**/*.*")
        .pipe(gulp.dest(options.SCRIPTS_OUT));

    
     var result_sass=gulp.src(options.SASS_IN+"/**/*.*")
        .pipe(gulp.dest(options.SASS_OUT));

       
     var result_images=gulp.src(options.IMAGES_IN+"/**/*.*")
        .pipe(gulp.dest(options.IMAGES_OUT));



/// Views

     var result_workspace=gulp.src(options.WORKSPACE_IN+"/**/*.*")
             .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
             .pipe(gulp.dest(options.WORKSPACE_OUT));


     var result_fragment=gulp.src(options.FRAGMENTS_IN+"/**/*.*")
             .pipe(gulp.dest(options.FRAGMENTS_OUT));

//gulp
      var result_php=gulp.src(options.GULP_IN+"/**/*.*")
       .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
       .pipe(concat(options.NAME+"_set_env.js"))
             .pipe(gulp.dest(options.GULP_OUT));

 



    var execSync = require('child_process').execSync;
    execSync('php artisan make:controller '+options.DNAME+'/'+"index");
    console.log("done controller");

    var part_1 = "public  function"+" _"+options.NAME+"()"+"{";
    var part_2 = "return view("+"\""+"aa_ServerPart.aa_WorkSpace"+"."+options.DNAME+".main"+"\""+"); }";

    var dest = "app/Http/Controllers/"+options.DNAME+'/'+"index.php";
    execSync( 'sed -i \'/{/a '+part_1+part_2+"\'"+'   '+dest);

    console.log("Done function injection in controller")

    //


   // console.log("A");
    if(options.DBASE !== undefined)
    {
      //  console.log("B");
        make_dbase_template(options);
    }



    const dest_root = "routes/web.php";
    const dummy = "Route::get(";
    const dest_part_1='Route::get(\"/'+options.NAME+'\",'+"\""+options.DNAME+'\\'+"index"+'@\_'+options.NAME+'\");';
    fs.appendFileSync(dest_root, dest_part_1);
    console.log("done rote  Injection _____________::"+dest_part_1);






    return result_sass && result_scripts && result_images && result_workspace 
            && result_fragment && result_php && result_php;


};




global.make_dbase_template = function (options)
{




    var execSync = require('child_process').execSync;
    execSync('php artisan make:migration '+options.NAME+" --create="+options.DBASE+"s");
    console.log("done migration");




    var dbase_C = options.DBASE.charAt(0).toUpperCase()+ options.DBASE.slice(1);
    var dbase_model =dbase_C.substring(0, dbase_C.length - 1);
    execSync('php artisan make:model '+"/Models/"+options.DNAME+"/"+dbase_model);
    console.log("done Model");


    execSync('php artisan make:seeder '+options.NAME);
    console.log("done Seeding");
    var part_1 = "$factory=factory(App\\"+dbase_model+"::class,30);";
    var part_2 = " $factory->create();";

    var dest = "database/seeds/"+options.NAME+".php";
    execSync( 'sed -i \'/{/a '+part_1+part_2+"\'"+'   '+dest);
    console.log("done Seed Injection");






    execSync('php artisan test-factory-helper:generate');
    console.log("done Factory");

    return execSync;




};








global.remove_template = function (dir,files,name)
{
    var result;
    var i;
    var execSync = require('child_process').execSync;


    var del = require('del');


    console.log(dir[2][1]);
    console.log(dir.length);

    for (i = 0; i < dir.length; i++) {
        console.log(dir[i][1]);

        result=fs.remove(dir[i][1], err => {
            if (err) return console.error(err)


        });
    }



    for (i = 0; i < files.length; i++) {
        console.log("*"+files[i][1]);
        del.sync(files[i][1]);

        console.log("done Factory"+files[i][1]);
    }

    var route ='sed -i '+'/'+name+'/d '+'./routes/web.php';


    execSync(route );
    execSync('php artisan config:cache');
    execSync('php artisan cache:clear');
    execSync('composer dump-autoload');
    execSync('php artisan migrate');







     
  return result;

    

};
