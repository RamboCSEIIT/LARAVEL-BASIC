 
 
var del                                                  = require('del');


//use this for deletion
var CSS_OUT_PATH                                  = 'ServerPart/public/00_CSS/**/*.*';
var SCRIPTS_OUT_PATH                          = 'ServerPart/public/01_SCRIPTS/**/*.*';
var VIEW_PATH_OUT_BLADE                                   = 'ServerPart/Views/**/*.*';
var IMAGES_OUT_PATH                            = 'ServerPart/public/02_IMAGES/**/*.*';
 



module.exports =
        {

            clean_without_image: function ()
            {


                var result = del.sync([
                    CSS_OUT_PATH, SCRIPTS_OUT_PATH, VIEW_PATH_OUT_BLADE
                ]);


                return result;

            },
            clean_with_image: function ()
            {


                var result = del.sync([
                    IMAGES_OUT_PATH
                ]);


                return result;

            },
                        test_global: function ()
            {


                var options = {
                    devBuild: true,
                    minify: false,
                    watch: false
                };

                cssTask(options);


            }




        };