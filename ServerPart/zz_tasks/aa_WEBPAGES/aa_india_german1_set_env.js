var SCRIPTS_OUT_PATH                                     = 'public/101_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '101_SCRIPTS/AA_INDIA_GERMAN1/**/*.*';
var SCRIPTS_OUT_FILE_NAME                              = 'aa_india_german1.js';


var SASS_OUT_PATH                                             = 'public/100_CSS';
var SASS_IN_PATH                                           = '100_SASS/AA_INDIA_GERMAN1/style.scss';
var SASS_OUT_FILE_NAME                                                = 'aa_india_german1.css';

var VIEW_T_IN_FRAG                              = '103_ViewsT/ab_Fragments/AA_INDIA_GERMAN1/**/*.*';
var VIEW_T_OUT_BODY                        = '103_ViewsT/aa_ServerPart/ab_body/AA_INDIA_GERMAN1';
 

module.exports = 
{
  
  aa_india_german1_mode: function () 
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  }
  
  
  
};