var SCRIPTS_OUT_PATH                                     = 'public/101_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '101_SCRIPTS/ZZ_TEST/**/*.*';
var SCRIPTS_OUT_FILE_NAME                              = 'zz_test.js';


var SASS_OUT_PATH                                             = 'public/100_CSS';
var SASS_IN_PATH                                           = '100_SASS/ZZ_TEST/style.scss';
var SASS_OUT_FILE_NAME                                                = 'zz_test.css';

var VIEW_T_IN_FRAG                              = '103_ViewsT/ab_Fragments/ZZ_TEST/**/*.*';
var VIEW_T_OUT_BODY                        = '103_ViewsT/aa_ServerPart/ab_body/ZZ_TEST';
 

module.exports = 
{
  
  zz_test_mode: function () 
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  }
  
  
  
};